const onHeaders = require("on-headers");
const HttpStatus = require("http-status-codes");
const logger = require("bbcw-logger");
const requestIp = require("request-ip");

module.exports = function (config) {
  return (req, res, next) => {
    const log = logger(config, req);
    const startDateTime = new Date();
    const remoteAddr = requestIp.getClientIp(req);
    const referer = req.header("referer");

    onHeaders(res, function () {
      const status = this.statusCode || 500;

      const message = {
        type: "inbound-service-call",
        request: {
          method: req.method,
          uri: req.originalUrl,
          remoteAddr,
          referer,
        },
        response: {
          status,
        },
        duration: new Date() - startDateTime,
        err: res.err ? res.err.stack : undefined,
      };
      const statusText = HttpStatus.getStatusText(status);
      if (res.statusCode < 400) {
        log.info(message, statusText);
      } else if (res.statusCode < 500) {
        log.warn(message, statusText);
      } else {
        log.error(message, statusText);
      }
    });

    next();
  };
};
